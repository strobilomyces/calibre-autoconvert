# calibre-autoconvert
Monitors a directory for ebooks with a certain format and automatically converts them to another (e.g. epub->mobi).

### Requirements
* calibre
* inotify-tools
* libxml-xpath-perl
* unzip

### Usage
* Configure the directory to watch, the formats to watch for, and the formats to convert to. Take care not to establish rules that might chain (pdf->epub, epub->mobi) or create an infinite loop (epub->mobi, mobi->epub); inotify looks for any new files in a directory, and that includes the file that was just converted.
* Run `bash calibre-autoconvert.sh`
* When a file matching the input format lands in the watched directory, the file is converted.
* For epub files, you can optionally pull the author/title from the metadata (when valid). The output format is: `Author - Title.extension`.

If you want the script to run in the background, add an `&` to the command.
To have it run on startup, you could add it to `/etc/rc.local` or `/etc/init.d`.

### Troubleshooting
* Make sure you're running the script with bash, and not sh.
* If you're running it in the background, you can see error messages and output in `/var/log/syslog`
* If you get an "End-of-central-directory signature not found" error, adjust the sleep time (default is 3 seconds) to something higher. There's a gap between the time inotifywait sees the event and when the file is created; waiting a few seconds resolves the bug.
* To see if your epub has valid metadata, run: `unzip -p your.epub OEBPS/content.opf | grep 'dc:title'`. If the command returns a line of XML, it's valid. Otherwise, something's not right.

### Notes
* Epub metadata is validated according to International Digital Publishing Forum spec 3.1 (http://www.idpf.org/epub/31/spec/epub-spec-20170105.html)
