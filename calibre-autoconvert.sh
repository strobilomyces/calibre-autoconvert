#!/bin/bash
# Author: Sean Ewing
# Project: https://github.com/strobilomyces/calibre-autoconvert
# Description: Monitors a directory for ebooks with a certain format
#   and automatically converts them to another (e.g. epub->mobi)
#   For epub, it optionally grabs metadata from the input file
#   to create files of the form: Author - Title.extension


# Configuration section
directory='/home/sean/Ebooks/Fiction' # Full path
declare -A conversion_list=( ["epub"]="mobi" \
                             ["pdf"]="mobi" )
rename_from_metadata='yes'
add_to_calibre_database='yes'
# End of Configuration Section


inotifywait -m -r -e create $directory | while read path action file; do

  sleep 5 # Wait for the file to be created.

  file_extension=${file#*.}
  file_extension=${file_extension,,} # convert to lowercase

  for c in ${!conversion_list[@]}; do

    if [ "$file_extension" == "$c" ]; then

      if [ "$file_extension" == "epub" ] && [ "$rename_from_metadata" == "yes" ]; then

        author="$(unzip -qq -p $path$file OEBPS/content.opf | xpath -q -e "//dc:creator/text()")"
        title="$(unzip -qq -p $path$file OEBPS/content.opf | xpath -q -e "//dc:title/text()")"

        if [ -n "$author" ] && [ -n "$title" ]; then
          converted_filename="$path$author - $title.${conversion_list[$c]}" # trims trailing whitespace
        else
          echo "$file has improperly formatted metadata. Reverting to existing format."
          converted_filename="$path${file%.*}.${conversion_list[$c]}"
        fi

      else
        converted_filename="$path${file%.*}.${conversion_list[$c]}"
      fi

      if ! test -f "$converted_filename"; then
        echo "Converting $file to $converted_filename"
        ebook-convert "$path$file" "$converted_filename" &>/dev/null
        if [ $? -eq 0 ]; then
          echo "Successfully converted $converted_filename."
        else
          echo "Error converting $converted_filename."
        fi

        if [ "$add_to_calibre_database" == "yes" ]; then
          calibredb add "$converted_filename" &>/dev/null
          if [ $? -eq 0 ]; then
            echo "Added $converted_filename to Calibre's database."
          else
            echo "Error adding $converted_filename to Calibre."
          fi
        fi
      else
        echo "$converted_filename already exists. Skipping."
      fi

    fi

  done

done
